SELECT * FROM employee WHERE atasan_id IS NULL ;

SELECT * FROM employee WHERE atasan_id=4 OR atasan_id=5;

SELECT * FROM employee WHERE atasan_id=1;

SELECT * FROM employee WHERE atasan_id=2 OR atasan_id=3;

with recursive cte (id, nama, atasan_id, company_id) as (
  select     id,
             nama,
             atasan_id,
             company_id
  from       employee
  where      nama = 'Pak Budi'
  union all
  select     p.id,
             p.nama,
             p.atasan_id,
             p.company_id
  from       employee p
  inner join cte
          on p.atasan_id = cte.id
)
select  count(id) as jumlah_bawahan from cte where nama <> 'Pak Budi';

with recursive cte (id, nama, atasan_id, company_id) as (
  select     id,
             nama,
             atasan_id,
             company_id
  from       employee
  where      nama = 'Bu Sinta'
  union all
  select     p.id,
             p.nama,
             p.atasan_id,
             p.company_id
  from       employee p
  inner join cte
          on p.atasan_id = cte.id
)
select  count(id) as jumlah_bawahan from cte where nama <> 'Bu Sinta';



