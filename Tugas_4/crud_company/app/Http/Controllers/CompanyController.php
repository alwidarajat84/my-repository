<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
        $company = Company::all();
        return view('index',['company' => $company]);
    }

    public function tambah()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        Company::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect('/company');
    }

    public function edit($id)
    {

        $company = Company::find($id);
        return view('edit',['company' => $company]);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        $company = Company::find($id);
        $company->nama = $request->nama;
        $company->alamat = $request->alamat;
        $company->save();
        return redirect('/company');
    }

    public function delete($id)
    {
        $company = Company::find($id);
        $company->delete();
        return redirect('/company');

    }
}
