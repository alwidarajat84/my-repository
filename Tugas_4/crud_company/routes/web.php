<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/company','App\Http\Controllers\CompanyController@index');
Route::get('company/tambah','App\Http\Controllers\CompanyController@tambah');
Route::post('/company/store', 'App\Http\Controllers\CompanyController@store');
Route::get('/company/edit/{id}','App\Http\Controllers\CompanyController@edit');
Route::put('/company/update/{id}', 'App\Http\Controllers\CompanyController@update');
Route::get('/company/hapus/{id}', 'App\Http\Controllers\CompanyController@delete');
