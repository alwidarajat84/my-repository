<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Many To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            Tambah Data Film
        </div>
        <div class="card-body">
            <a href="/film" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>

            <form method="post" action="/film/store">

                {{ csrf_field() }}

                <div class="form-group">
                    <label>Film</label>
                    <input type="text" name="film" class="form-control" placeholder="Nama film ..">

                    @if($errors->has('film'))
                        <div class="text-danger">
                            {{ $errors->first('film')}}
                        </div>
                    @endif

                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
