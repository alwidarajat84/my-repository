<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Many To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            EDIT DATA FILM
        </div>
        <div class="card-body">
            <a href="/film" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>


            <form method="post" action="/film/update/{{ $film->id }}">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label>film</label>
                    <input type="text" name="film" class="form-control" placeholder="Nama film .."
                           value=" {{ $film->film }}">

                    @if($errors->has('film'))
                        <div class="text-danger">
                            {{ $errors->first('film')}}
                        </div>
                    @endif

                </div>


                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
