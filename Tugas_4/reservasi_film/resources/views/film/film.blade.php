<!DOCTYPE html>
<html>
<head>
    <title>Relasi Many To Many</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <div style="margin-top: 40px">
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="/reservasi">Reservasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/film">Film</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/penonton">Penonton</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="card mt-5">
        <div class="card-body">
            <h3>Film</h3>
            <a href="/film/tambah" class="btn btn-primary">Tambah Data Film</a>
            <table class="table table-bordered table-hover table-striped " style="text-align: center">
                <thead>
                <tr>
                    <th>Film</th>
                    <th>opsi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($film as $f)
                    <tr>
                        <td>{{ $f->film }}</td>
                        <td>
                            <a href="/film/edit/{{ $f->id }}" class="btn btn-warning">Edit</a>
                            <a href="/film/hapus/{{ $f->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

</body>
</html>
