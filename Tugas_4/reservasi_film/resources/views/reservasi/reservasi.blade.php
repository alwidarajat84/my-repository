<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Many To Many</title>
</head>
<body>
<div class="container">
    <div style="margin-top: 40px">
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link active" href="/reservasi">Reservasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/film">Film</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/penonton">Penonton</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="card mt-5">
        <div class="card-header text-center">
            Reservasi
        </div>
        <div class="card-body">
            <a href="/reservasi/tambah" class="btn btn-primary">Reservasi</a>
            <br/>
            <br/>
            <table class="table table-bordered table-hover table-striped" style="text-align: center">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Film</th>
                    <th>Penonton</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody >
                @foreach($reservasi as $d)
                    <tr>
                        <td>{{$d->id}}</td>
                        <td>{{$d->film->film}}</td>
                        <td>{{$d->penonton->nama}}</td>
                        <td>
                            <a href="/reservasi/edit/{{ $d->id }}" class="btn btn-warning">Edit</a>
                            <a href="/reservasi/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
