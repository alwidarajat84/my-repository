<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Many To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            Tambah Data Penonton
        </div>
        <div class="card-body">
            <a href="/reservasi" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>

            <form method="post" action="/reservasi/store">

                {{ csrf_field() }}

                <div class="form-group">
                    <select class="form-control select2" name="film_id" id="film_id">
                        <option  value>Film</option>
                        @foreach($film as $f)
                            <option value="{{$f->id}}">{{$f->film}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select class="form-control select2" name="penonton_id" id="penonton_id">
                        <option  value>Penonton</option>
                        @foreach($penonton as $p)
                            <option value="{{$p->id}}">{{$p->nama}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
