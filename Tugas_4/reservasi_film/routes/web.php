<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//penonton
Route::get('/penonton','App\Http\Controllers\PenontonController@index');
Route::get('/penonton/tambah', 'App\Http\Controllers\PenontonController@tambah');
Route::post('/penonton/store', 'App\Http\Controllers\PenontonController@store');
Route::get('/penonton/edit/{id}', 'App\Http\Controllers\PenontonController@edit');
Route::put('/penonton/update/{id}', 'App\Http\Controllers\PenontonController@update');
Route::get('/penonton/hapus/{id}', 'App\Http\Controllers\PenontonController@delete');

//film
Route::get('/film','App\Http\Controllers\FilmController@index');
Route::get('/film/tambah', 'App\Http\Controllers\FilmController@tambah');
Route::post('/film/store', 'App\Http\Controllers\FilmController@store');
Route::get('/film/edit/{id}', 'App\Http\Controllers\FilmController@edit');
Route::put('/film/update/{id}', 'App\Http\Controllers\FilmController@update');
Route::get('/film/hapus/{id}', 'App\Http\Controllers\FilmController@delete');

//reservasi
Route::get('/reservasi','App\Http\Controllers\FilmPenontonController@index');
Route::get('/reservasi/tambah', 'App\Http\Controllers\FilmPenontonController@tambah');
Route::post('/reservasi/store', 'App\Http\Controllers\FilmPenontonController@store');
Route::get('/reservasi/edit/{id}', 'App\Http\Controllers\FilmPenontonController@edit');
Route::put('/reservasi/update/{id}', 'App\Http\Controllers\FilmPenontonController@update');
Route::get('/reservasi/hapus/{id}', 'App\Http\Controllers\FilmPenontonController@delete');

