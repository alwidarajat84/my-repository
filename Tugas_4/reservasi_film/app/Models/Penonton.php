<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    protected $table = "penonton";
    protected $fillable = ['nama'];

    public function film()
    {
        return $this->belongsToMany(Film::class,
            'film_penonton','film_id','penonton_id');
    }
    public function film_penonton(){
        return $this->hasMany(Film_Penonton::class);
    }
}
