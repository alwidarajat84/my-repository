<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = "film";
    protected $fillable = ['film'];

    public function penonton()
    {
        return $this->belongsToMany(Penonton::class,
            'film_penonton','film_id','penonton_id');
    }
    public function film_penonton(){
        return $this->hasMany(Film_Penonton::class);
    }
}
