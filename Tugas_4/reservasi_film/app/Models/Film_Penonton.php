<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film_Penonton extends Model
{
    protected $table = "film_penonton";
    protected $fillable = ['film_id','penonton_id'];

    public function film(){
        return $this->belongsTo(Film::class);
    }
    public function penonton(){
        return $this->belongsTo(Penonton::class);
    }
}
