<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film_Penonton;
use App\Models\Film;
use App\Models\Penonton;

class FilmPenontonController extends Controller
{
    public function index()
    {
        $reservasi = Film_Penonton::all();
        return view('reservasi.reservasi', ['reservasi' => $reservasi]);
    }

    public function tambah()
    {
        $film = Film::all();
        $penonton = Penonton::all();
        return view('reservasi.tambah',compact('film','penonton'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'film_id' => 'required',
            'penonton_id' => 'required'
        ]);

        Film_Penonton::create([
            'film_id' => $request->film_id,
            'penonton_id' => $request->penonton_id
        ]);

        return redirect('/reservasi');
    }
    public function edit($id)
    {
        $film = Film::all();
        $penonton = Penonton::all();
        $reservasi = Film_Penonton::find($id);
        return view('reservasi.edit', compact('film','penonton','reservasi'));
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'film_id' => 'required',
            'penonton_id' => 'required'
        ]);

        $reservasi = Film_Penonton::find($id);
        $reservasi->film_id = $request->film_id;
        $reservasi->penonton_id = $request->penonton_id;
        $reservasi->save();
        return redirect('/reservasi');
    }
    public function delete($id)
    {
        $reservasi = Film_Penonton::find($id);
        $reservasi->delete();
        return redirect('/reservasi');
    }
}
