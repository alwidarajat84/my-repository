<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penonton;

class PenontonController extends Controller
{
    public function index()
    {
        $penonton = Penonton::all();
        return view('penonton.penonton', ['penonton' => $penonton]);
    }

    public function tambah()
    {
        return view('penonton.tambah');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required'
        ]);

        Penonton::create([
            'nama' => $request->nama
        ]);

        return redirect('/penonton');
    }
    public function edit($id)
    {
        $penonton = Penonton::find($id);
        return view('penonton.edit', ['penonton' => $penonton]);
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'nama' => 'required'
        ]);

        $penonton = Penonton::find($id);
        $penonton->nama = $request->nama;
        $penonton->save();
        return redirect('/penonton');
    }
    public function delete($id)
    {
        $penonton = Penonton::find($id);
        $penonton->delete();
        return redirect('/penonton');
    }
}
