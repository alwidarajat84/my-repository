<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;

class FilmController extends Controller
{
    public function index()
    {
        $film = Film::all();
        return view('film.film', ['film' => $film]);
    }

    public function tambah()
    {
        return view('film.tambah');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'film' => 'required'
        ]);

        Film::create([
            'film' => $request->film
        ]);

        return redirect('/film');
    }
    public function edit($id)
    {
        $film = Film::find($id);
        return view('film.edit', ['film' => $film]);
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'film' => 'required'
        ]);

        $film = Film::find($id);
        $film->film = $request->film;
        $film->save();
        return redirect('/film');
    }
    public function delete($id)
    {
        $film = Film::find($id);
        $film->delete();
        return redirect('/film');
    }
}
