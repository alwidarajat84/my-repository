<li>{{ $child_category->nama }}</li>
@if ($child_category->employee)
    <ul>
        @foreach ($child_category->employee as $childCategory)
            @include('child_category', ['child_category' => $childCategory])
        @endforeach
    </ul>
@endif
