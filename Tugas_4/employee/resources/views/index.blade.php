<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Employee</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            CRUD Employee
        </div>
        <div class="card-body">
            <a href="/employee/tambah" class="btn btn-primary">Tambah Data</a>
            <br/>
            <a href="/employee/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Atasan</th>
                    <th>Company</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employee as $e)
                    <tr>
                        <td>{{ $e->id}}</td>
                        <td>{{$e->nama }}</td>
                        <td>{{optional($e->atasan)->nama}}</td>
                        <td>{{ $e->company->nama}}</td>
                        <td>
                            <a href="/employee/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                            <a href="/employee/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
