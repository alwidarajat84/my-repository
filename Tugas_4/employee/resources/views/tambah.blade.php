<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>CRUD Employee</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            Tambah Data Employee
        </div>
        <div class="card-body">
            <a href="/employee" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>

            <form method="post" action="/employee/store">

                {{ csrf_field() }}

                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama ..">

                    @if($errors->has('nama'))
                    <div class="text-danger">
                        {{ $errors->first('nama')}}
                    </div>
                    @endif

                </div>

                <div class="form-group">
                    <label>Atasan ID</label>
                    <textarea name="atasan_id" class="form-control" placeholder="Atasan ID .."></textarea>

                    @if($errors->has('atasan_id'))
                    <div class="text-danger">
                        {{ $errors->first('atasan_id')}}
                    </div>
                    @endif

                </div>

                <div class="form-group">
                    <label>Company ID</label>
                    <textarea name="company_id" class="form-control" placeholder="Company ID .."></textarea>

                    @if($errors->has('company_id'))
                    <div class="text-danger">
                        {{ $errors->first('company_id')}}
                    </div>
                    @endif

                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
