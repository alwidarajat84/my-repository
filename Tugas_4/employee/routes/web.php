<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('employee','App\Http\Controllers\EmployeeController@index');
Route::get('employee/tambah','App\Http\Controllers\EmployeeController@tambah');
Route::post('/employee/store', 'App\Http\Controllers\EmployeeController@store');
Route::get('/employee/edit/{id}','App\Http\Controllers\EmployeeController@edit');
Route::put('/employee/update/{id}', 'App\Http\Controllers\EmployeeController@update');
Route::get('/employee/hapus/{id}', 'App\Http\Controllers\EmployeeController@delete');
Route::get('/employee/export_excel', 'App\Http\Controllers\EmployeeController@export_excel');
