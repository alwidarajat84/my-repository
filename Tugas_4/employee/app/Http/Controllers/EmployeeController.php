<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
class EmployeeController extends Controller
{
    public function index()
    {

        $employees = Employee::with('atasan','bawahan')->get();
        //dd($employees);

        return view('index',['employee' => $employees]);
    }
        
    public function export_excel()
    {
        return Excel::download(new EmployeeExport, 'employee.xlsx');
    }

    public function tambah()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);

        Employee::create([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id
            ]);

        return redirect('/employee');
    }

    public function edit($id)
    {

        $employee = Employee::find($id);
        return view('edit',['employee' => $employee]);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);

        $employee = Employee::find($id);
        $employee->nama = $request->nama;
        $employee->atasan_id = $request->atasan_id;
        $employee->company_id = $request->company_id;
        $employee->save();
        return redirect('/employee');
    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect('/employee');

    }

}
