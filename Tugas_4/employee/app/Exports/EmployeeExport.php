<?php

namespace App\Exports;

use App\Employee;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Concerns\FromCollection;

class EmployeeExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \App\Models\Employee::all();
    }
}
