<?php

namespace App\Models;

use App\Http\Controllers\EmployeeController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "company";
    protected $fillable = ['nama','alamat'];

    public function Employee(){
        return $this->hasMany(Employee::class);
    }
}
