<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = "employee";
    protected $fillable = ['nama','atasan_id','company_id'];

    public function Company(){
        return $this->belongsTo(Company::class);
    }

    public function atasan()
    {
        return $this->belongsTo(Employee::class);
    }

    public function bawahan()
    {
        return $this->hasMany(Employee::class,'atasan_id');
    }


}
