<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/provinsi','App\Http\Controllers\ProvinsiController@index');
Route::get('/provinsi/tambah','App\Http\Controllers\ProvinsiController@tambah');
Route::post('/provinsi/store', 'App\Http\Controllers\ProvinsiController@store');
Route::get('/provinsi/edit/{id}', 'App\Http\Controllers\ProvinsiController@edit');
Route::put('/provinsi/update/{id}', 'App\Http\Controllers\ProvinsiController@update');
Route::get('/provinsi/hapus/{id}', 'App\Http\Controllers\ProvinsiController@delete');


Route::get('/kabupaten','App\Http\Controllers\KabupatenController@index');
Route::get('/kabupaten/tambah','App\Http\Controllers\KabupatenController@tambah');
Route::post('/kabupaten/store', 'App\Http\Controllers\KabupatenController@store');
Route::get('/kabupaten/edit/{id}', 'App\Http\Controllers\KabupatenController@edit');
Route::put('/kabupaten/update/{id}', 'App\Http\Controllers\KabupatenController@update');
Route::get('/kabupaten/hapus/{id}', 'App\Http\Controllers\KabupatenController@delete');

Route::get('/kecamatan','App\Http\Controllers\KecamatanController@index');
Route::get('/kecamatan/tambah','App\Http\Controllers\KecamatanController@tambah');
Route::post('/kecamatan/store', 'App\Http\Controllers\KecamatanController@store');
Route::get('/kecamatan/edit/{id}', 'App\Http\Controllers\KecamatanController@edit');
Route::put('/kecamatan/update/{id}', 'App\Http\Controllers\KecamatanController@update');
Route::get('/kecamatan/hapus/{id}', 'App\Http\Controllers\KecamatanController@delete');

Route::get('/desa','App\Http\Controllers\DesaController@index');
Route::get('/desa/tambah','App\Http\Controllers\DesaController@tambah');
Route::post('/desa/store', 'App\Http\Controllers\DesaController@store');
Route::get('/desa/edit/{id}', 'App\Http\Controllers\DesaController@edit');
Route::put('/desa/update/{id}', 'App\Http\Controllers\DesaController@update');
Route::get('/desa/hapus/{id}', 'App\Http\Controllers\DesaController@delete');


