<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $fillable = ['kecamatan','kabupaten_id','provinsi_id'];

    public function provinsi(){
        return $this->belongsTo(Provinsi::class);
    }

    public function kabupaten(){
        return $this->belongsTo(Kabupaten::class);
    }

    public function desa(){
        return $this->hasMany(Desa::class);
    }
}
