<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $fillable = ['nama'];

    public function kabupaten(){
        return $this->hasMany(Provinsi::class);
    }

    public function kecamatan(){
        return $this->hasMany(Kecamatan::class);
    }

    public function desa(){
        return $this->hasMany(Desa::class);
    }

}
