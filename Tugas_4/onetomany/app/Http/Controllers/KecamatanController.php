<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use App\Models\Provinsi;
use App\Models\Kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function index()
    {
        $kecamatan = Kecamatan::all();
        return view('kecamatan.kecamatan', ['kecamatan' => $kecamatan]);
    }

    public function tambah()
    {
        $kabupaten = Kabupaten::all();
        $provinsi = Provinsi::all();
        return view('kecamatan.tambah',compact('provinsi','kabupaten'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'kecamatan' => 'required',
            'kabupaten_id' => 'required',
            'provinsi_id' => 'required'
        ]);

        Kecamatan::create([
            'kecamatan' => $request->kecamatan,
            'kabupaten_id' => $request->kabupaten_id,
            'provinsi_id' => $request->provinsi_id

        ]);


        return redirect('/kecamatan');
    }


    public function edit($id)
    {
        $kabupaten = Kabupaten::all();
        $provinsi = Provinsi::all();
        $kecamatan = Kecamatan::with('kabupaten','provinsi')->find($id);
        return view('kecamatan.edit', compact('kecamatan','kabupaten','provinsi'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'kecamatan' => 'required',
            'kabupaten_id' => 'required',
            'provinsi_id' => 'required'
        ]);

        $kecamatan = Kecamatan::find($id);
        $kecamatan->kecamatan = $request->kecamatan;
        $kecamatan->kabupaten_id = $request->kabupaten_id;
        $kecamatan->provinsi_id = $request->provinsi_id;
        $kecamatan->save();
        return redirect('/kecamatan');
    }

    public function delete($id)
    {
        $kecamatan = Kecamatan::find($id);
        $kecamatan->delete();
        return redirect('/kecamatan');
    }
}
