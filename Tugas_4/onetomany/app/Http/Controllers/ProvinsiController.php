<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinsi;

class ProvinsiController extends Controller
{
    public function index()
    {
        $provinsi = Provinsi::all();
        return view('provinsi.provinsi', ['provinsi' => $provinsi]);
    }

    public function tambah()
    {
        return view('provinsi.tambah');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required'
        ]);

        Provinsi::create([
            'nama' => $request->nama
        ]);


        return redirect('/provinsi');
    }


    public function edit($id)
    {
        $provinsi = Provinsi::find($id);
        return view('provinsi.edit', ['provinsi' => $provinsi]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'nama' => 'required'
        ]);

        $provinsi = Provinsi::find($id);
        $provinsi->nama = $request->nama;
        $provinsi->save();
        return redirect('/provinsi');
    }

    public function delete($id)
    {
        $provinsi = Provinsi::find($id);
        $provinsi->delete();
        return redirect('/provinsi');
    }

}
