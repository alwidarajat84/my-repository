<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kabupaten;
use App\Models\Provinsi;

class KabupatenController extends Controller
{
    public function index()
    {
        $kabupaten = Kabupaten::all();
        return view('kabupaten.kabupaten', ['kabupaten' => $kabupaten]);
    }

    public function tambah()
    {
        $provinsi = Provinsi::all();
        return view('kabupaten.tambah',compact('provinsi'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'provinsi_id' => 'required'
        ]);

        Kabupaten::create([
            'nama' => $request->nama,
            'provinsi_id' => $request->provinsi_id

        ]);


        return redirect('/kabupaten');
    }


    public function edit($id)
    {
        $provinsi = Provinsi::all();
        $kabupaten = Kabupaten::with('provinsi')->find($id);
        return view('kabupaten.edit', compact('kabupaten','provinsi'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'provinsi_id' => 'required'
        ]);

        $kabupaten = Kabupaten::find($id);
        $kabupaten->nama = $request->nama;
        $kabupaten->provinsi_id = $request->provinsi_id;
        $kabupaten->save();
        return redirect('/kabupaten');
    }

    public function delete($id)
    {
        $kabupaten = Kabupaten::find($id);
        $kabupaten->delete();
        return redirect('/kabupaten');
    }
}
