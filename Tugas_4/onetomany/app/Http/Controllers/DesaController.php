<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Provinsi;
use App\Models\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    public function index()
    {
        $desa = Desa::all();
        //dd($desa->all());
        return view('desa.desa', compact('desa'));
    }

    public function tambah()
    {
        $kecamatan = Kecamatan::all();
        $kabupaten = Kabupaten::all();
        $provinsi = Provinsi::all();
        return view('desa.tambah',compact('provinsi','kabupaten','kecamatan'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'desa' => 'required',
            'kecamatan_id' => 'required',
            'kabupaten_id' => 'required',
            'provinsi_id' => 'required'
        ]);

        Desa::create([
            'desa' => $request->desa,
            'kecamatan_id' => $request->kecamatan_id,
            'kabupaten_id' => $request->kabupaten_id,
            'provinsi_id' => $request->provinsi_id

        ]);


        return redirect('/desa');
    }


    public function edit($id)
    {
        $kecamatan = Kecamatan::all();
        $kabupaten = Kabupaten::all();
        $provinsi = Provinsi::all();
        $desa = Desa::with('kecamatan','kabupaten','provinsi')->find($id);
        return view('desa.edit', compact('desa','kecamatan','kabupaten','provinsi'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            'desa' => 'required',
            'kecamatan_id' => 'required',
            'kabupaten_id' => 'required',
            'provinsi_id' => 'required'
        ]);

        $desa = Desa::find($id);
        $desa->desa = $request->desa;
        $desa->kecamatan_id = $request->kecamatan_id;
        $desa->kabupaten_id = $request->kabupaten_id;
        $desa->provinsi_id = $request->provinsi_id;
        $desa->save();
        return redirect('/desa');
    }

    public function delete($id)
    {
        $desa = Desa::find($id);
        $desa->delete();
        return redirect('/desa');
    }
}
