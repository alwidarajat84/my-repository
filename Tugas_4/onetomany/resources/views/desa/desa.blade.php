<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            CRUD Data Desa
        </div>
        <div class="card-body">
            <a href="/desa/tambah" class="btn btn-primary">Tambah Data</a>
            <br/>
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Desa</th>
                    <th>Kecamatan</th>
                    <th>Kabupaten</th>
                    <th>Provinsi</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody>
                @foreach($desa as $d)
                <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->desa}}</td>
                    <td>{{$d->kecamatan->kecamatan}}</td>
                    <td>{{$d->kabupaten->nama}}</td>
                    <td>{{$d->provinsi->nama}}</td>
                    <td>
                        <a href="/desa/edit/{{ $d->id }}" class="btn btn-warning">Edit</a>
                        <a href="/desa/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
