<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            EDIT DATA
        </div>
        <div class="card-body">
            <a href="/desa" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>


            <form method="post" action="/desa/update/{{ $desa->id }}">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label>Desa</label>
                    <input type="text" name="desa" class="form-control" placeholder="Nama desa .."
                           value=" {{ $desa->desa }}">

                    @if($errors->has('desa'))
                        <div class="text-danger">
                            {{ $errors->first('desa')}}
                        </div>
                    @endif

                </div>
                <div class="form-group">
                    <select class="form-control select2" name="kecamatan_id" id="kecamatan_id">
                        <option value="{{ $desa->kecamatan_id }}">{{ $desa->kecamatan->kecamatan }}</option>
                        @foreach($kecamatan as $k)
                            <option value="{{$k->id}}">{{$k->kecamatan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select2" name="kabupaten_id" id="kabupaten_id">
                        <option value="{{ $desa->kabupaten_id }}">{{ $desa->kabupaten->nama }}</option>
                        @foreach($kabupaten as $p)
                            <option value="{{$p->id}}">{{$p->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select2" name="provinsi_id" id="provinsi_id">
                        <option value="{{ $desa->provinsi_id }}">{{ $desa->provinsi->nama }}</option>
                        @foreach($provinsi as $p)
                            <option value="{{$p->id}}">{{$p->nama}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
