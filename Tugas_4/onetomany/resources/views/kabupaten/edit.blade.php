<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            EDIT DATA
        </div>
        <div class="card-body">
            <a href="/kabupaten" class="btn btn-primary">Kembali</a>
            <br/>
            <br/>


            <form method="post" action="/kabupaten/update/{{ $kabupaten->id }}">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label>kabupaten</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama kabupaten .."
                           value=" {{ $kabupaten->nama }}">

                    @if($errors->has('nama'))
                        <div class="text-danger">
                            {{ $errors->first('nama')}}
                        </div>
                    @endif

                </div>
                <div class="form-group">
                    <select class="form-control select2" name="provinsi_id" id="provinsi_id">
                        <option value="{{ $kabupaten->provinsi_id }}">{{ $kabupaten->provinsi->nama }}</option>
                        @foreach($provinsi as $p)
                            <option value="{{$p->id}}">{{$p->nama}}</option>
                        @endforeach
                    </select>


                </div>


                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>

            </form>

        </div>
    </div>
</div>
</body>
</html>
