<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            CRUD Data Kabupaten
        </div>
        <div class="card-body">
            <a href="/kabupaten/tambah" class="btn btn-primary">Tambah Data</a>
            <br/>
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Kabupaten</th>
                    <th>Provinsi</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody>
                @foreach($kabupaten as $p)
                <tr>
                    <td>{{$p->id}}</td>
                    <td>{{$p->nama }}</td>
                    <td>{{$p->provinsi->nama}}</td>
                    <td>
                        <a href="/kabupaten/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                        <a href="/kabupaten/hapus/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
