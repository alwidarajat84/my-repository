<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            CRUD Data Provinsi
        </div>
        <div class="card-body">
            <a href="/provinsi/tambah" class="btn btn-primary">Tambah Data</a>
            <br/>
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Provinsi</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody>
                @foreach($provinsi as $p)
                <tr>
                    <td>{{$p->id}}</td>
                    <td>{{ $p->nama }}</td>
                    <td>
                        <a href="/provinsi/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                        <a href="/provinsi/hapus/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
