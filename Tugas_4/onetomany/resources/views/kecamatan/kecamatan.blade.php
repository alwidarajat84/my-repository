<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>One To Many</title>
</head>
<body>
<div class="container">
    <div class="card mt-5">
        <div class="card-header text-center">
            CRUD Data Kecamatan
        </div>
        <div class="card-body">
            <a href="/kecamatan/tambah" class="btn btn-primary">Tambah Data</a>
            <br/>
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Kecamatan</th>
                    <th>Kabupaten</th>
                    <th>Provinsi</th>
                    <th>OPSI</th>
                </tr>
                </thead>
                <tbody>
                @foreach($kecamatan as $k)
                <tr>
                    <td>{{$k->id}}</td>
                    <td>{{$k->kecamatan }}</td>
                    <td>{{$k->kabupaten->nama}}</td>
                    <td>{{$k->provinsi->nama}}</td>
                    <td>
                        <a href="/kecamatan/edit/{{ $k->id }}" class="btn btn-warning">Edit</a>
                        <a href="/kecamatan/hapus/{{ $k->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
