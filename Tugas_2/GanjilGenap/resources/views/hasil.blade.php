<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table border="2px">
    @for($no = $angka1; $no <= $angka2; $no++)
        <tr>
             <td>
                 @if($no%2==0)
                    Angka {{$no}} adalah genap<br>

                 @else
                     Angka {{$no}}  adalah ganjil<br>
                 @endif

             </td>
        </tr>
    @endfor
</table>

</body>
</html>
