<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CekController extends Controller
{
    public function index()
    {
        return view('cek');
    }
    public function cek(Request $request)
    {
        $angka1 = $request->input('angka1');
        $angka2 = $request->input('angka2');
        return view('hasil',['angka1' => $angka1, 'angka2' => $angka2]);
    }

}
