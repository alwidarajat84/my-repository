<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form  method="post">
    {{csrf_field()}}
    <label>Masukan Angka pertama: </label><br>
    <input name="angka1" type="text"><br>
    <label>Masukan Angka kedua: </label><br>
    <input name="angka2" type="text"><br>
    <button type="submit"  formaction="/kalkulator/tambah">+</button>
    <button type="submit"  formaction="/kalkulator/kurang">-</button>
    <button type="submit"  formaction="/kalkulator/kali">x</button>
    <button type="submit"  formaction="/kalkulator/bagi">/</button>

</form>

</body>
</html>
