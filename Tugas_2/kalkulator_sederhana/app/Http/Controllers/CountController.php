<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CountController extends Controller
{
     public function index()
        {
            return view('index');
        }
        public function tambah(Request $request)
        {
            $angka1 = $request->input('angka1');
            $angka2 = $request->input('angka2');
            $result = $angka1 + $angka2;
            return $result;
        }
        public function kurang(Request $request)
        {
            $angka1 = $request->input('angka1');
            $angka2 = $request->input('angka2');
            $result = $angka1 - $angka2;
            return $result;
        }

        public function kali(Request $request)
        {
            $angka1 = $request->input('angka1');
            $angka2 = $request->input('angka2');
            $result = $angka1 * $angka2;
            return $result;
        }

        public function bagi(Request $request)
        {
            $angka1 = $request->input('angka1');
            $angka2 = $request->input('angka2');
            $result = $angka1 / $angka2;
            return $result;
        }
}
