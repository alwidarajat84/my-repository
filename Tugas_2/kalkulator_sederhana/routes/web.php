<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/kalkulator','App\Http\Controllers\CountController@index');
Route::post('/kalkulator/tambah','App\Http\Controllers\CountController@tambah');
Route::post('/kalkulator/kurang','App\Http\Controllers\CountController@kurang');
Route::post('/kalkulator/kali','App\Http\Controllers\CountController@kali');
Route::post('/kalkulator/bagi','App\Http\Controllers\CountController@bagi');
